#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 27 15:41:47 2022

@author: pi
"""
import RPi.GPIO as GPIO
import function_Ultrasonic as fu
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import serial#******** lee los datos que envia la pico
from time import sleep
import datetime

#********************************* serial setups **************
ser = serial.Serial('/dev/ttyACM0',9600)
ser.flushInput()
#********************** setups **********
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

Trig1 = 21   #pin 23 como Trig                              
Echo1 = 20   #pin 24 como Echo
GPIO.setup(Trig1,GPIO.OUT)    #TRIG como pin de salida        
GPIO.setup(Echo1,GPIO.IN)

Trig2 = 15   #pin 23 como Trig                              
Echo2 = 14   #pin 24 como Echo
GPIO.setup(Trig2,GPIO.OUT)    #TRIG como pin de salida        
GPIO.setup(Echo2,GPIO.IN)

water_level = 0
#************************** Motors **********************

motorC = 16
GPIO.setup(motorC,GPIO.OUT)
motorP = 12
GPIO.setup(motorP,GPIO.OUT)

led_water = 1
GPIO.setup(led_water,GPIO.OUT)
act3 = False



#*********************************************************************

def generate_data():
    data1 = fu.distance(Trig1, Echo1)
    data2 = fu.distance(Trig2, Echo2)
    #print(data)
    sleep(1)
    return data1,data2#datos a firebase

def state_act():
    var1,Var2 = False,False
    dis_container = fu.distance(Trig1, Echo1) 
    dis_plate = fu.distance(Trig2, Echo2)

    if(dis_container >= 30):
        var1 = True
        GPIO.output(motorC,True)
        #print("Act_Containter ON")
    else:
        var1 = False
        GPIO.output(motorC,False)
        #print("Act_Containter OFF")
        
    if(dis_plate >= 7):
        var2 = True
        GPIO.output(motorP,True)
        #print("Act_Plate ON")
    else:
        var2 = False
        GPIO.output(motorP,False)
        #print("Act_Plate OFF")
        
    return var1, var2          #datos a firebase
    
    
    
def generate_time(initial_time):
    final_time= datetime.datetime.now()
    delta = final_time - initial_time
    return delta.total_seconds()

initial_time= datetime.datetime.now()

try:
    app = firebase_admin.get_app()
except ValueError as e:
    # Fetch the service account key JSON file contents
    cred = credentials.Certificate('first_json.json')
    # Initialize the app with a service account, granting admin privileges
    firebase_admin.initialize_app(cred, {'databaseURL': 'https://fir-test-71900-default-rtdb.firebaseio.com/'})

# *********************Create estructure to sensors*************************
ref_sens = db.reference("sensors")
ref_sens.set({'sensor1':{'data': 0.0,'time': 0.0 },
              'sensor2':{'data': 0.0,'time': 0.0 },
              'sensor3':{'data': 0.0,'time': 0.0}})
# update sensors
ref_sens = db.reference('sensors')

# *******************************Create estructure to actuators**********************
ref_act = db.reference("actuators")
ref_act.set({'actuator_1':{'data': 0.0,'time': 0.0 },
             'actuator_2':{'data': 0.0,'time': 0.0},
             'actuator_3':{'data': 0.0,'time': 0.0}})
# update sensors
ref_act = db.reference('actuators')

trigger = True

while True:
   
    try:
#***************************** data pico***************
        lineBytes = ser.readline() #lectura de USB
        line = lineBytes.decode('utf-8').strip()
        water_level = int(line)
        #print(type(water_level))
        if ( water_level <= 20000):
            act3 = True
            GPIO.output(led_water, True)
        else:
            GPIO.output(led_water, False)
            act3 = False
#************************** send firebase sensors **************
        # Sensor 1
        sensor1,sensor2 = generate_data()
        time1 =  generate_time(initial_time)
        time2 =  generate_time(initial_time)
        time3 =  generate_time(initial_time)
        ref_sens.update({
            'sensor1/data': sensor1,
            'sensor1/time': time1,
            'sensor2/data': sensor2,
            'sensor2/time': time2,
            'sensor3/data': water_level,
            'sensor3/time': time3
        })
        
#************************** send firebase actuators **************
        
        # Actuator 1-2
        act1,act2 = state_act()
        time1 =  str(datetime.datetime.now())
        time2 =  str(datetime.datetime.now())
        time3 =  str(datetime.datetime.now())
        ref_act.update({
            'actuator_1/data': act1,    #MODO SET
            'actuator_1/time': time1,
            'actuator_2/data': act2,
            'actuator_2/time': time2,
            'actuator_3/data': act3,
            'actuator_3/time': time3
        })
          
        # print(ref_sens.get())
        # print("\n")
        # print("print actuators")
        # print(ref_act.get())
        # print("\n")
        
    except KeyboardInterrupt:
       break