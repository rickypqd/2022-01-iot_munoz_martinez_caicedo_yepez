import RPi.GPIO as GPIO                    
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

def distance(triger, echo):
    distancia = float
    v_sonido= 34300
    GPIO.setup(triger,GPIO.OUT)    #TRIG como pin de salida        
    GPIO.setup(echo,GPIO.IN)
    
    GPIO.output(triger, True)     #TRIG en estado alto          
    time.sleep(0.00001)         #Delay de 10 microsegundos           
    GPIO.output(triger, False)    #TRIG en estado bajo
    
    while GPIO.input(echo)==0:    
        start = time.time()  # tiempo de inicio de pulso
        
    while GPIO.input(echo)==1:
        end = time.time()   # tiempo de fin de pulso
        
    tiempo = end - start   #Se obtienen la duración del pulso, calculando la diferencia entre inicio y fin del pulso
    distancia =  v_sonido * (tiempo/2)    #calculamos la distancia                 
    distancia = round(distancia, 2)     #se redondea a dos decimales
    return distancia
    #print(distancia)
    
