#include <NewPing.h>
NewPing hc_sr04(22,23,300);//******* trig=22--echo=23---distance max=300

TaskHandle_t hc_sr04task, joysticktask;
//*********************************** vars ******************
float distance_cm;
int x=2,y=4,read_x,read_y;
//**************************************************
void ultrasonic(void*parameter){
  while(true){
    distance_cm=hc_sr04.ping_cm();
    Serial.println("distance is: " + String(distance_cm)+ " in nucle: " + String(xPortGetCoreID()));
    delay(210);
  }
  vTaskDelay(12);
}
void joystick(void*parameter){
  while(true){
    read_x=analogRead(x);read_y=analogRead(y);
    Serial.println("\t\t\t value x is : " + String(read_x)+ "\t\t value in y is: "+ String(read_y)+" in nucle: " + String(xPortGetCoreID()));
    delay(180);
  }
  vTaskDelay(12);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); 
  xTaskCreatePinnedToCore(ultrasonic,"ultrasonic",10000,NULL,1,&hc_sr04task,0);
  xTaskCreatePinnedToCore(joystick,"joystick",10000,NULL,1,&joysticktask,1);
} 

void loop() {
  // put your main code here, to run repeatedly:

}
