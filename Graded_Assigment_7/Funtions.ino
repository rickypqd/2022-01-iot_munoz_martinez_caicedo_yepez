void wifiInit(){
  Serial.println("Connecting to...");
  Serial.println(ssid);

  WiFi.begin(ssid,password);

  while(WiFi.status() != WL_CONNECTED){
    Serial.print("..");
    delay(540);
  }
  Serial.println("Conect to wifi");
  Serial.println("Addres IP");
  Serial.println(WiFi.localIP());
}

void reconnect(){
  while(!mqttClient.connected()){
    Serial.println("Conecting with MQTT");
    if(mqttClient.connect("arduinoClient")){
      Serial.print("Connected");
      //mqttClient.subscribe("esp/32");//************ es donde se suscribe al al topic dado  ***********
    }
    else{
      Serial.print("Fail, rc= ");
      Serial.println(mqttClient.state());
      Serial.println("intentar de nuevo");
      delay(5400);
    }
  }
}
