#include <DHT.h>// ********************* libreria sensor temperatura ***************
#include <DHT_U.h>

#include <NewPing.h>
NewPing hc_sr04(27,26,630); //*********(trig,echo,distance_Max)*****
#include <PubSubClient.h>//************** libreria subscribirse y publicar ***************
#include <WiFiClientSecure.h>
#include <WiFi.h>

WiFiClient esp32;//****************** se crea un objeto *****************
PubSubClient mqttClient(esp32);//************* crea un funcion  se le pasa por parametro el cliente wifi

//************************** credentials ************
const char* ssid = "HUAWEI Y9 Prime 2019";
const char* password = "rickypqd";
//************************** client setups broker *********
char *server = "broker.emqx.io";
int port = 1883;
//********************* variables*********************
int var=0, ultrasonic=0 ,randi=0, sensor_dht=14,temp=0;
char datos_ultrasonic [45];
char datos_dht[45];
char datos_random[39];
String resultS = "";
//************* crate object dht ************
DHT dht(sensor_dht , DHT22);///************** sensitive case ***********
//************************* Tasks ****************
TaskHandle_t task_1;//*********** name of task *************************
TaskHandle_t task_2;
//****************** funtions tasks *********************//
void loop1 (void*parameter){// **** primer ciclo para uno de los nucleos***
  while(true){//************************* ciclo infinito ********************
    if(!mqttClient.connected()){
    reconnect();
    }
    //********************* data of ultrasonic *****************
    ultrasonic = hc_sr04.ping_cm();
    Serial.print(" Data of ultrasonic is: ");
    Serial.println(ultrasonic);
    //********************** data of dht *****************
    temp = dht.readTemperature();
    Serial.print(" Data of Dht Temperature is : ");
    Serial.println(temp);
    delay(6000);
    //***************** send data from ultrasonic *********
    sprintf(datos_ultrasonic, "  data ultra sonic is: %d  ", ultrasonic);
    mqttClient.publish("topic/grupo3/sensor1",datos_ultrasonic);
    delay(1000);
    //************* send data from dht temp *********
    sprintf(datos_dht, "  data dht temperature is: %d ", temp);
    mqttClient.publish("topic/grupo3/sensor2",datos_dht);
    delay(1000);
  }
  vTaskDelay(18);
}
  
void loop2(void*parameter){
  while(true){
  }
  vTaskDelay(18);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  dht.begin();
  delay(9);
  wifiInit();
  mqttClient.setServer(server,port);
  //**************** tasks **************************
  xTaskCreatePinnedToCore(
    loop1,//**loop name////
    "task1",//** aleatore name ***
    10000,//** space in memory**
    NULL,//*** NULL ****
    1,//** priority**
    &task_1,//**task name**
    0//** nucle number **
    );// crea el core donde vamos a trabajar Parametros( loop_creado,aleatore_name,espacio_de_Memoria_NULL,Prioridad,&nombre_tarea,nucleo a ejecutar)
  xTaskCreatePinnedToCore(loop2,"task2",10000,NULL,2,&task_2,1);
  //************************************************************************
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
