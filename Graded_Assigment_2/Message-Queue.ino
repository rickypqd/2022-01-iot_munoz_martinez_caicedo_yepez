
/*Integrantes:
 * Nicolle Daniela Muñoz Perez
 * Juan David Martínez Bermudez
 * Nelson Ricardo Caicedo Rivera
 * Iván Darío Yépez Moreno
*/
  
//Sensor Temperatura
#include <DHT.h>//https://github.com/adafruit/DHT-sensor-library
#define DHTPIN 4  
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

//Potenciometer
const int portPin = 2; //pin potenciometro

/* structure that hold data*/
typedef struct{
  float sender;
  float datasensor;
  int counter;
}Data;

/* this variable hold queue handle */
xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore;

void setup() {
  Serial.begin(9600);
  //Serial.println("Bluetooth Device is Ready to Pair");
  dht.begin();
  
  xBinarySemaphore = xSemaphoreCreateBinary();
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(Data));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1);                    /* Task handle to keep track of created task */
}

//******************MAIN LOOP**********
void loop() {

}

//*****************TASK 1*********************************
void sendTask1( void * parameter ){
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data;
  /* sender 1 has id is 1 */
  data.sender = 1;

  for(;;){
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    //data.datasensor = random(1,10);
    data.datasensor = dht.readTemperature();
    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
    Serial.println("\t\t\t\t Sensor 1 - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* check whether sending is ok or not */
    if( xStatus == pdPASS ) {
      /* increase counter of sender 1 */
      data.counter = data.counter + 1;
    }
    /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(1000));// Simulated interruption
  }
  vTaskDelete( NULL );
}
//*******************************************************************************
/* this task is similar to sendTask1 */
//*****************TASK 2*********************************
void sendTask2( void * parameter ) {
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  data.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    //data.datasensor =random(1,10);
    data.datasensor = analogRead(portPin);
    //Serial.println("sendTask2 is sending data");
    Serial.println("\t\t\t\t Sensor 2 - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    if( xStatus == pdPASS ) {
      data.counter = data.counter + 1;
    }
   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(1000));// Simulated interruption
  }
  vTaskDelete( NULL );
}

//***************************************************************************************
void receiveTask( void * parameter ){
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &data, xTicksToWait );
    /* check whether receiving is ok or not */
    if(xStatus == pdPASS){
      /* print the data to terminal */
      Serial.print("Visualizing data: ");
      Serial.print("Sensor = ");
      Serial.print(data.sender);
      Serial.print(" with data = ");
      Serial.println(data.datasensor);
    }
  }
  vTaskDelete( NULL );
}
