#include <DHT.h>
#include <DHT_U.h>

#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

//************** create obeject *********************
RF24 radio(7,8);//******* (ce,csn)
//*************** variables *******************
const byte ID[6]= "00006";// ****direccion del canal de comunicación
char date[16];
int var[16];

int sensor = 2;      // pin DATA de DHT22 a pin digital 2
int temp[16];
int pot[16];
DHT dht(sensor, DHT22);  

struct MyData 
{
  float pot;
  float temp;
};
MyData data;

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
 radio.begin();
 radio.openWritingPipe(ID);//*** se asigna el canal de comunicacion a esta funcion
 radio.setPALevel(RF24_PA_MAX);//****** potencia de comunicacion 
 radio.setDataRate(RF24_250KBPS);
 radio.setChannel(100);
 //radio.stopListening();//************ para que deje de recivir datos
 //pinMode(3,INPUT);
 pinMode(A0,INPUT);
 dht.begin();
}

void loop() {
 // put your main code here, to run repeatedly:
 data.pot = analogRead(A0);
 data.temp = dht.readTemperature();
 //temp[0] = dht.readTemperature();  
 //pot[1] = dht.readTemperature(); 
 //pot[0] = analogRead(A0);
 radio.write(&data, sizeof(MyData));
 //radio.write(pot,sizeof(pot));
 //Serial.println(pot);
 //Data Temperature
 /*radio.write(temp,sizeof(temp));
 Serial.print("    Temperatura: ");  // escritura en monitor serial de los valores
 Serial.println(temp[0]);*/
 delay(1000);
 //state=digitalRead(3);
 
}
