#include<WiFi.h>
#include<ThingSpeak.h>
//************************** credenciales ***************************
const char* ssid= "CAICEDO_RIVERA";
const char* password= "22261419";

//************************** nrfl*************************************
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>
//************** create object *********************
RF24 radio(4,5);//******* (ce,csn)
//*************** variables *******************
const byte ID[6]= "00006";

WiFiClient client;

unsigned long myChannelNumber = 1666934;
const char* myWriteAPIKey = "C2GVYMDC3XBKAC7O";
//*************************** variable ******************************

//****************************** struct *****************************************
struct MyData 
{
  float pot;
  float temp;
};
MyData data;
//********************************_______Tasks_______***********************************************

TaskHandle_t task_1;//*********** name of task *************************
TaskHandle_t task_2;

void loop1 (void*parameter){// **** es necesario del esp 32***
  while(1){
    if(radio.available()){
    //radio.read(var,sizeof(var));// leer el dato que llega desde el emisor
      radio.read(&data, sizeof(MyData));
      Serial.println((String)"Pot: " + data.pot); 
      Serial.println((String)"Temp: " + data.temp); 
  }
  ///////****************************************************************************************************
    vTaskDelay(pdMS_TO_TICKS(10000));
  }
  vTaskDelay(NULL);
}
void loop2(void*parameter){
  while(true){
    Serial.println("Data Waiting...");
    ThingSpeak.setField(1,data.pot);
    ThingSpeak.setField(2,data.temp);
    Serial.println();
    delay(10000);
  
    int x= ThingSpeak.writeFields(myChannelNumber,myWriteAPIKey);
    if(x == 200){
      Serial.println("Channel Update Successful");
  }else{
      Serial.println("Problem update" + String(x));
  }
  vTaskDelay(pdMS_TO_TICKS(10000));
}
  vTaskDelay(NULL);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(1,ID);//*** se asigna a cuantos modulos se va a conectar y  el canal de comunicacion a esta funcion 
  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_LOW);//****** potencia de comunicacion 
  radio.startListening();//************ para que el modulo siempre este escuchando
  radio.setChannel(100);
  Serial.println("Void Setup OK");
  //********************** wife setup *****************
  WiFi.mode(WIFI_STA);
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("....");
    while (WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid,password);
      delay(10000);
    }
    Serial.println("Conected");
    
  }
  
  ThingSpeak.begin(client);
  
  xTaskCreatePinnedToCore(
    loop1,//**loop name////
    "task1",//** aleatore name ***
    10000,//** space in memory**
    NULL,//*** NULL ****
    1,//** priority**
    &task_1,//**task name**
    0//** nucle number **
    );// crea el core donde vamos a trabajar Parametros( loop_creado,aleatore_name,espacio_de_Memoria_NULL,Prioridad,&nombre_tarea,nucleo a ejecutar)
  xTaskCreatePinnedToCore(loop2,"task2",10000,NULL,2,&task_2,1);
}

void loop() {
  // put your main code here, to run repeatedly:
}
