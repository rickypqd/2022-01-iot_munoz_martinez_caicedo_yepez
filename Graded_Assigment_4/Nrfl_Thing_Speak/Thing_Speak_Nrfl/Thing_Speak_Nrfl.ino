/*Integrantes:
 * Nicolle Daniela Muñoz Perez
 * Juan David Martínez Bermudez
 * Nelson Ricardo Caicedo Rivera
 * Iván Darío Yépez Moreno
*/
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>
//************** create obeject *********************
RF24 radio(4,5);//******* (ce,csn)
//*************** variables *******************
const byte ID[6]= "00001";
int var[16];

#include <WiFi.h>
//#include<WiFiMulti.h>
#include "secrets.h"
#include "ThingSpeak.h" // always include thingspeak header file after other header files and custom macros

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;
String myStatus = "";
/* structure that hold data*/
typedef struct Data{
  float sender;
  float datasensor;
};
//*************** struct nrfl2401 ***************
struct MyData {
  float pot;
  float temp;
};
MyData data;
/* this variable hold queue handle */
xQueueHandle xQueue;

void setup() {
  Serial.begin(9600);
  WiFi.mode(WIFI_STA);   
  ThingSpeak.begin(client);  // Initialize ThingSpeak
  
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(Data));
  xTaskCreatePinnedToCore(sendTask1, "sendTask1", 10000,NULL,2,NULL,0);
  xTaskCreatePinnedToCore(sendTask2,"sendTask2",10000,NULL,2,NULL,0);
  xTaskCreatePinnedToCore(receiveTask,"receiveTask",10000,NULL,1,NULL,1);
  //******************** radio setup *****************
  radio.begin();
  radio.openReadingPipe(1,ID);//*** se asigna a cuantos modulos se va a conectar y  el canal de comunicacion a esta funcion 
  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_LOW);//****** potencia de comunicacion 
  radio.startListening();//************ para que el modulo siempre este escuchando
  radio.setChannel(100);
  Serial.println("Void Setup OK");
}

void loop() {}
