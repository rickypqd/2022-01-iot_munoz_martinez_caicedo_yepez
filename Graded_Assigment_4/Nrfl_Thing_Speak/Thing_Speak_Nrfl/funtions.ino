//***************** funtions **********************
void sendTask1( void * parameter )
{
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data;
  /* sender 1 has id is 1 */
  data.sender = 1;

  while(true){
    if(radio.available()){
    radio.read(&data, sizeof(Data));
    Serial.println("\t\t\t\t Pot - Adding: "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* we delay here so that receiveTask has chance to receive data */
    vTaskDelay(pdMS_TO_TICKS(10000));
    //data.datasensor = data.pot;
    //Serial.println((String)"pot: " + data.pot); 
    //Serial.println((String)"temp: " + data.temp);  
  }
    
    /*Serial.println("\t\t\t\t Pot - Adding: "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* we delay here so that receiveTask has chance to receive data */
    //vTaskDelay(pdMS_TO_TICKS(10000));
  }
  vTaskDelete( NULL );
}
/* this task is similar to sendTask1 */
void sendTask2( void * parameter )
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  data.sender = 2;
  
  while(true){
    if(radio.available()){
    radio.read(&data, sizeof(Data));
    //data.datasensor = data.pot;
    //Serial.println((String)"pot: " + data.pot); 
    //Serial.println((String)"temp: " + data.temp);  
  }
    //Serial.println("sendTask2 is sending data");
    Serial.println("\t\t\t\t temp - Adding"+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /*if( xStatus == pdPASS ) {
      //data.counter = data.counter + 1;
    }*/
    vTaskDelay(pdMS_TO_TICKS(10000));
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter ){
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data dataReceive;
  Data dataReceive1;
  Data dataReceive2;
  
  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }
  
  while(true){
    // Use WiFiClient class to create TCP connections
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &dataReceive, xTicksToWait );
    /* check whether receiving is ok or not */
    if(xStatus == pdPASS){
      /* print the data to terminal */
      Serial.print("receiveTask got data: ");
      Serial.print("sender = ");
      Serial.print(dataReceive.sender);
      Serial.print(" data = ");
      Serial.println(dataReceive.datasensor);
      if(dataReceive.sender==1){dataReceive1 = dataReceive;}
      if(dataReceive.sender==2){dataReceive2 = dataReceive;}
    }

    //xStatus = xQueueReceive( xQueue, &dataReceive, xTicksToWait );
    /* check whether receiving is ok or not */
    //if(xStatus == pdPASS){
      /* print the data to terminal */
    /*Serial.print("receiveTask got data: ");
    Serial.print("sender = ");
    Serial.print(dataReceive.sender);
    Serial.print(" data = ");
    Serial.println(dataReceive.datasensor);
    if(dataReceive.sender==1){dataReceive1 = dataReceive;}
    if(dataReceive.sender==2){dataReceive2 = dataReceive;}
    }*/
    
    Serial.println("Connected Client"); 
   // set the fields with the values
    ThingSpeak.setField(1, dataReceive1.datasensor);
    ThingSpeak.setField(2, dataReceive2.datasensor);
        
  // set the status
  ThingSpeak.setStatus(myStatus);
  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }

  vTaskDelay(pdMS_TO_TICKS(20000));  
    }
    vTaskDelete( NULL );
  }
