#include <DHT.h>
#include <DHT_U.h>

#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

//************** create obeject *********************
RF24 radio(7,8);//******* (ce,csn)
//*************** variables *******************
const byte ID[6]= "00003";// ****direccion del canal de comunicación
int sensor = 2;      // pin DATA de DHT22 a pin digital 2

DHT dht(sensor, DHT22);  

struct MyData 
{
  float pot;
  float temp;
};
MyData data;

void setup() {
  // put your setup code here, to run once:
 Serial.begin(115200);
 radio.begin();
 radio.openWritingPipe(ID);//*** se asigna el canal de comunicacion a esta funcion
 radio.setPALevel(RF24_PA_LOW);//****** potencia de comunicacion 
 radio.setDataRate(RF24_250KBPS);
 radio.setChannel(100);
 pinMode(A0,INPUT);
 dht.begin();
}

void loop() {
 // put your main code here, to run repeatedly:
 data.pot = analogRead(A0);
 data.temp = dht.readTemperature();
 radio.write(&data, sizeof(MyData));
 delay(1000);
 
}
